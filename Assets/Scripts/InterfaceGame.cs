﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InterfaceGame : MonoBehaviour
{

	public Text textHealth;
	public Text textCoins;
	public Text textPower;
	public Text textTime;
	
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		textHealth.text = "HEALTH: " + GameManager.health + "/" + GameManager.totalHealth;
		textCoins.text = "COINS: " + GameManager.collectedCoins + "/" + GameManager.totalCoins;
		textPower.text = "POWER: " + GameManager.power + "/" + GameManager.totalPower;
		textTime.text = "TIME: " + GameManager.time + " sec";
	}
}
