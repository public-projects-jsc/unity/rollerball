﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Impulse : MonoBehaviour
{

	private AudioSource audio;
	public float impulseForce = 200.0f;
	// Use this for initialization
	void Start () {
		GetComponent<Rigidbody>().AddForce(Vector3.back * impulseForce, ForceMode.Impulse);
		audio = GetComponent<AudioSource>();
		audio.Play();
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.position.y <= 0.2) {
			Destroy(gameObject);
		}
	}
}
