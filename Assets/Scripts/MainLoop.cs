﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainLoop : MonoBehaviour
{

	private AudioSource audioComplete;
	private bool stillPlaying = true;
	
	// Use this for initialization
	void Start () {
		audioComplete = GetComponent<AudioSource>();
		StartCoroutine(Play());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	IEnumerator Play() {

		while (stillPlaying) {
			if (GameManager.collectedCoins == GameManager.totalCoins) {
				print("CONGRATS");
				audioComplete.Play();
				stillPlaying = false;
			}

			if (GameManager.health <= 0) {
				print("DEAD");
				stillPlaying = false;
			}
		}
		
		return null;
	}
}
