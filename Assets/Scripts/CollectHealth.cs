﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectHealth : MonoBehaviour {

	private AudioSource audio;
	
	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	private void OnTriggerEnter(Collider other) {
		if (GameManager.health != GameManager.totalHealth) {
			
			audio.Play();

			if (GameManager.health + 20 <= GameManager.totalHealth) {
				GameManager.health += 20;
			
			} else {
				GameManager.health = GameManager.totalHealth;
			}
		
			gameObject.GetComponent<MeshRenderer>().enabled = false;
			Destroy(gameObject,1f);
		}
		
	}
}
