﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Eruption : MonoBehaviour
{

	public GameObject stone;
	public float fireRate = 0.5f;
	
	void Start ()
	{
		StartCoroutine(ThrowStone());
	}
	
	// Update is called once per frame
	void Update () {
	}

	IEnumerator ThrowStone()
	{
		yield return new WaitForSeconds(2.0f);
		
		while (true)
		{
			Instantiate(stone, transform.position,Random.rotation);
			yield return new WaitForSeconds(fireRate);
		}
	}
}
