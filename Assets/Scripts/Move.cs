﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Experimental.UIElements;
using UnityEngine.Playables;

public class Move : MonoBehaviour
{

	public Material normalMaterial;
	public Material godMaterial;
	public float forceValue;
	public float jumpValue;
	private Rigidbody rigidbody;
	public AudioSource audio;
	public AudioClip godAudio;
	public AudioClip jumpAudio;
	public AudioClip painAudio;
	public Button godModeBtn;
	private bool godMode;
	
	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody>();
		audio = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {

		GameManager.time += Time.deltaTime;
		GameManager.time = float.Parse(Math.Round(GameManager.time, 2,MidpointRounding.ToEven).ToString());
		
		if (Input.GetButtonDown("Jump") && Mathf.Abs(rigidbody.velocity.y) < 0.01f) {
			rigidbody.AddForce(Vector3.up * jumpValue, ForceMode.Impulse);
			audio.PlayOneShot(jumpAudio);
		}

		if (GameManager.collectedCoins == GameManager.totalCoins || GameManager.health <= 0) {
			Application.LoadLevel("FinalScene");
		}

		if (Input.GetKeyDown(KeyCode.E) && GameManager.power == GameManager.totalPower) {
			StartGodMode();
		}

		if (transform.position.y < -10f) {
			Application.LoadLevel("FinalScene");
		}
	}

	void FixedUpdate() {
		rigidbody.AddForce(new Vector3(Input.GetAxis("Horizontal"),
										0,
										Input.GetAxis("Vertical")) * forceValue);
	}

	private void OnCollisionEnter(Collision collision) {
		if (collision.gameObject.CompareTag("CannonBall") && !godMode) { 
			GameManager.health -= 20;
			audio.PlayOneShot(painAudio);
		}
	}

	public void StartGodMode() {

		if (GameManager.power == GameManager.totalPower) {
			audio.PlayOneShot(godAudio);
			gameObject.GetComponent<MeshRenderer>().material = godMaterial;
			godMode = true;
			StartCoroutine(GodMode());
		}
	}

	IEnumerator GodMode() {
		yield return new WaitForSeconds(5);

		godMode = false;
		gameObject.GetComponent<MeshRenderer>().material = normalMaterial;
	}
}
