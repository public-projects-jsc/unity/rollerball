﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectTime : MonoBehaviour {

	private AudioSource audio;
	
	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	private void OnTriggerEnter(Collider other) {
		audio.Play();
		GameManager.time -= 5f;
		gameObject.GetComponent<MeshRenderer>().enabled = false;
		Destroy(gameObject,1f);
	}
}
